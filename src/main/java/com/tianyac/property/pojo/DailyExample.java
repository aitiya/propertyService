package com.tianyac.property.pojo;

import java.util.ArrayList;
import java.util.List;

public class DailyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DailyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGuardianNameIsNull() {
            addCriterion("guardian_name is null");
            return (Criteria) this;
        }

        public Criteria andGuardianNameIsNotNull() {
            addCriterion("guardian_name is not null");
            return (Criteria) this;
        }

        public Criteria andGuardianNameEqualTo(String value) {
            addCriterion("guardian_name =", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameNotEqualTo(String value) {
            addCriterion("guardian_name <>", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameGreaterThan(String value) {
            addCriterion("guardian_name >", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameGreaterThanOrEqualTo(String value) {
            addCriterion("guardian_name >=", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameLessThan(String value) {
            addCriterion("guardian_name <", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameLessThanOrEqualTo(String value) {
            addCriterion("guardian_name <=", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameLike(String value) {
            addCriterion("guardian_name like", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameNotLike(String value) {
            addCriterion("guardian_name not like", value, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameIn(List<String> values) {
            addCriterion("guardian_name in", values, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameNotIn(List<String> values) {
            addCriterion("guardian_name not in", values, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameBetween(String value1, String value2) {
            addCriterion("guardian_name between", value1, value2, "guardianName");
            return (Criteria) this;
        }

        public Criteria andGuardianNameNotBetween(String value1, String value2) {
            addCriterion("guardian_name not between", value1, value2, "guardianName");
            return (Criteria) this;
        }

        public Criteria andTimeIsNull() {
            addCriterion("time is null");
            return (Criteria) this;
        }

        public Criteria andTimeIsNotNull() {
            addCriterion("time is not null");
            return (Criteria) this;
        }

        public Criteria andTimeEqualTo(String value) {
            addCriterion("time =", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotEqualTo(String value) {
            addCriterion("time <>", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeGreaterThan(String value) {
            addCriterion("time >", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeGreaterThanOrEqualTo(String value) {
            addCriterion("time >=", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeLessThan(String value) {
            addCriterion("time <", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeLessThanOrEqualTo(String value) {
            addCriterion("time <=", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeLike(String value) {
            addCriterion("time like", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotLike(String value) {
            addCriterion("time not like", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeIn(List<String> values) {
            addCriterion("time in", values, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotIn(List<String> values) {
            addCriterion("time not in", values, "time");
            return (Criteria) this;
        }

        public Criteria andTimeBetween(String value1, String value2) {
            addCriterion("time between", value1, value2, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotBetween(String value1, String value2) {
            addCriterion("time not between", value1, value2, "time");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityIsNull() {
            addCriterion("public_security is null");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityIsNotNull() {
            addCriterion("public_security is not null");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityEqualTo(String value) {
            addCriterion("public_security =", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityNotEqualTo(String value) {
            addCriterion("public_security <>", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityGreaterThan(String value) {
            addCriterion("public_security >", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityGreaterThanOrEqualTo(String value) {
            addCriterion("public_security >=", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityLessThan(String value) {
            addCriterion("public_security <", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityLessThanOrEqualTo(String value) {
            addCriterion("public_security <=", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityLike(String value) {
            addCriterion("public_security like", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityNotLike(String value) {
            addCriterion("public_security not like", value, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityIn(List<String> values) {
            addCriterion("public_security in", values, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityNotIn(List<String> values) {
            addCriterion("public_security not in", values, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityBetween(String value1, String value2) {
            addCriterion("public_security between", value1, value2, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andPublicSecurityNotBetween(String value1, String value2) {
            addCriterion("public_security not between", value1, value2, "publicSecurity");
            return (Criteria) this;
        }

        public Criteria andEquipmentIsNull() {
            addCriterion("equipment is null");
            return (Criteria) this;
        }

        public Criteria andEquipmentIsNotNull() {
            addCriterion("equipment is not null");
            return (Criteria) this;
        }

        public Criteria andEquipmentEqualTo(String value) {
            addCriterion("equipment =", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentNotEqualTo(String value) {
            addCriterion("equipment <>", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentGreaterThan(String value) {
            addCriterion("equipment >", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentGreaterThanOrEqualTo(String value) {
            addCriterion("equipment >=", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentLessThan(String value) {
            addCriterion("equipment <", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentLessThanOrEqualTo(String value) {
            addCriterion("equipment <=", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentLike(String value) {
            addCriterion("equipment like", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentNotLike(String value) {
            addCriterion("equipment not like", value, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentIn(List<String> values) {
            addCriterion("equipment in", values, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentNotIn(List<String> values) {
            addCriterion("equipment not in", values, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentBetween(String value1, String value2) {
            addCriterion("equipment between", value1, value2, "equipment");
            return (Criteria) this;
        }

        public Criteria andEquipmentNotBetween(String value1, String value2) {
            addCriterion("equipment not between", value1, value2, "equipment");
            return (Criteria) this;
        }

        public Criteria andRequiresIsNull() {
            addCriterion("requires is null");
            return (Criteria) this;
        }

        public Criteria andRequiresIsNotNull() {
            addCriterion("requires is not null");
            return (Criteria) this;
        }

        public Criteria andRequiresEqualTo(String value) {
            addCriterion("requires =", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresNotEqualTo(String value) {
            addCriterion("requires <>", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresGreaterThan(String value) {
            addCriterion("requires >", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresGreaterThanOrEqualTo(String value) {
            addCriterion("requires >=", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresLessThan(String value) {
            addCriterion("requires <", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresLessThanOrEqualTo(String value) {
            addCriterion("requires <=", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresLike(String value) {
            addCriterion("requires like", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresNotLike(String value) {
            addCriterion("requires not like", value, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresIn(List<String> values) {
            addCriterion("requires in", values, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresNotIn(List<String> values) {
            addCriterion("requires not in", values, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresBetween(String value1, String value2) {
            addCriterion("requires between", value1, value2, "requires");
            return (Criteria) this;
        }

        public Criteria andRequiresNotBetween(String value1, String value2) {
            addCriterion("requires not between", value1, value2, "requires");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Integer value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Integer value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Integer value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Integer value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Integer value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Integer> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Integer> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Integer value1, Integer value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}