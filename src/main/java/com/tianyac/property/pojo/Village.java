package com.tianyac.property.pojo;

public class Village {
    private Integer id;

    private String villageName;

    private Integer buildingNums;

    private Integer buildingHight;

    private Integer lineZoomNums;

    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName == null ? null : villageName.trim();
    }

    public Integer getBuildingNums() {
        return buildingNums;
    }

    public void setBuildingNums(Integer buildingNums) {
        this.buildingNums = buildingNums;
    }

    public Integer getBuildingHight() {
        return buildingHight;
    }

    public void setBuildingHight(Integer buildingHight) {
        this.buildingHight = buildingHight;
    }

    public Integer getLineZoomNums() {
        return lineZoomNums;
    }

    public void setLineZoomNums(Integer lineZoomNums) {
        this.lineZoomNums = lineZoomNums;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }
}