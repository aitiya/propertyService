package com.tianyac.property.pojo;

public class PropertyStatistics extends Property {
    private Integer population;

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }
}
