package com.tianyac.property.pojo;

import java.util.ArrayList;
import java.util.List;

public class VillageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VillageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andVillageNameIsNull() {
            addCriterion("village_name is null");
            return (Criteria) this;
        }

        public Criteria andVillageNameIsNotNull() {
            addCriterion("village_name is not null");
            return (Criteria) this;
        }

        public Criteria andVillageNameEqualTo(String value) {
            addCriterion("village_name =", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameNotEqualTo(String value) {
            addCriterion("village_name <>", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameGreaterThan(String value) {
            addCriterion("village_name >", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameGreaterThanOrEqualTo(String value) {
            addCriterion("village_name >=", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameLessThan(String value) {
            addCriterion("village_name <", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameLessThanOrEqualTo(String value) {
            addCriterion("village_name <=", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameLike(String value) {
            addCriterion("village_name like", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameNotLike(String value) {
            addCriterion("village_name not like", value, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameIn(List<String> values) {
            addCriterion("village_name in", values, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameNotIn(List<String> values) {
            addCriterion("village_name not in", values, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameBetween(String value1, String value2) {
            addCriterion("village_name between", value1, value2, "villageName");
            return (Criteria) this;
        }

        public Criteria andVillageNameNotBetween(String value1, String value2) {
            addCriterion("village_name not between", value1, value2, "villageName");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsIsNull() {
            addCriterion("building_nums is null");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsIsNotNull() {
            addCriterion("building_nums is not null");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsEqualTo(Integer value) {
            addCriterion("building_nums =", value, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsNotEqualTo(Integer value) {
            addCriterion("building_nums <>", value, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsGreaterThan(Integer value) {
            addCriterion("building_nums >", value, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsGreaterThanOrEqualTo(Integer value) {
            addCriterion("building_nums >=", value, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsLessThan(Integer value) {
            addCriterion("building_nums <", value, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsLessThanOrEqualTo(Integer value) {
            addCriterion("building_nums <=", value, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsIn(List<Integer> values) {
            addCriterion("building_nums in", values, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsNotIn(List<Integer> values) {
            addCriterion("building_nums not in", values, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsBetween(Integer value1, Integer value2) {
            addCriterion("building_nums between", value1, value2, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingNumsNotBetween(Integer value1, Integer value2) {
            addCriterion("building_nums not between", value1, value2, "buildingNums");
            return (Criteria) this;
        }

        public Criteria andBuildingHightIsNull() {
            addCriterion("building_hight is null");
            return (Criteria) this;
        }

        public Criteria andBuildingHightIsNotNull() {
            addCriterion("building_hight is not null");
            return (Criteria) this;
        }

        public Criteria andBuildingHightEqualTo(Integer value) {
            addCriterion("building_hight =", value, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightNotEqualTo(Integer value) {
            addCriterion("building_hight <>", value, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightGreaterThan(Integer value) {
            addCriterion("building_hight >", value, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightGreaterThanOrEqualTo(Integer value) {
            addCriterion("building_hight >=", value, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightLessThan(Integer value) {
            addCriterion("building_hight <", value, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightLessThanOrEqualTo(Integer value) {
            addCriterion("building_hight <=", value, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightIn(List<Integer> values) {
            addCriterion("building_hight in", values, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightNotIn(List<Integer> values) {
            addCriterion("building_hight not in", values, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightBetween(Integer value1, Integer value2) {
            addCriterion("building_hight between", value1, value2, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andBuildingHightNotBetween(Integer value1, Integer value2) {
            addCriterion("building_hight not between", value1, value2, "buildingHight");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsIsNull() {
            addCriterion("line_zoom_nums is null");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsIsNotNull() {
            addCriterion("line_zoom_nums is not null");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsEqualTo(Integer value) {
            addCriterion("line_zoom_nums =", value, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsNotEqualTo(Integer value) {
            addCriterion("line_zoom_nums <>", value, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsGreaterThan(Integer value) {
            addCriterion("line_zoom_nums >", value, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsGreaterThanOrEqualTo(Integer value) {
            addCriterion("line_zoom_nums >=", value, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsLessThan(Integer value) {
            addCriterion("line_zoom_nums <", value, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsLessThanOrEqualTo(Integer value) {
            addCriterion("line_zoom_nums <=", value, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsIn(List<Integer> values) {
            addCriterion("line_zoom_nums in", values, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsNotIn(List<Integer> values) {
            addCriterion("line_zoom_nums not in", values, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsBetween(Integer value1, Integer value2) {
            addCriterion("line_zoom_nums between", value1, value2, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andLineZoomNumsNotBetween(Integer value1, Integer value2) {
            addCriterion("line_zoom_nums not between", value1, value2, "lineZoomNums");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}