package com.tianyac.property.pojo;

import java.util.ArrayList;
import java.util.List;

public class ParkingLotExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ParkingLotExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardIsNull() {
            addCriterion("owner_id_card is null");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardIsNotNull() {
            addCriterion("owner_id_card is not null");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardEqualTo(String value) {
            addCriterion("owner_id_card =", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardNotEqualTo(String value) {
            addCriterion("owner_id_card <>", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardGreaterThan(String value) {
            addCriterion("owner_id_card >", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardGreaterThanOrEqualTo(String value) {
            addCriterion("owner_id_card >=", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardLessThan(String value) {
            addCriterion("owner_id_card <", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardLessThanOrEqualTo(String value) {
            addCriterion("owner_id_card <=", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardLike(String value) {
            addCriterion("owner_id_card like", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardNotLike(String value) {
            addCriterion("owner_id_card not like", value, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardIn(List<String> values) {
            addCriterion("owner_id_card in", values, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardNotIn(List<String> values) {
            addCriterion("owner_id_card not in", values, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardBetween(String value1, String value2) {
            addCriterion("owner_id_card between", value1, value2, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andOwnerIdCardNotBetween(String value1, String value2) {
            addCriterion("owner_id_card not between", value1, value2, "ownerIdCard");
            return (Criteria) this;
        }

        public Criteria andParkingtimeIsNull() {
            addCriterion("parkingtime is null");
            return (Criteria) this;
        }

        public Criteria andParkingtimeIsNotNull() {
            addCriterion("parkingtime is not null");
            return (Criteria) this;
        }

        public Criteria andParkingtimeEqualTo(String value) {
            addCriterion("parkingtime =", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeNotEqualTo(String value) {
            addCriterion("parkingtime <>", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeGreaterThan(String value) {
            addCriterion("parkingtime >", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeGreaterThanOrEqualTo(String value) {
            addCriterion("parkingtime >=", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeLessThan(String value) {
            addCriterion("parkingtime <", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeLessThanOrEqualTo(String value) {
            addCriterion("parkingtime <=", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeLike(String value) {
            addCriterion("parkingtime like", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeNotLike(String value) {
            addCriterion("parkingtime not like", value, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeIn(List<String> values) {
            addCriterion("parkingtime in", values, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeNotIn(List<String> values) {
            addCriterion("parkingtime not in", values, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeBetween(String value1, String value2) {
            addCriterion("parkingtime between", value1, value2, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingtimeNotBetween(String value1, String value2) {
            addCriterion("parkingtime not between", value1, value2, "parkingtime");
            return (Criteria) this;
        }

        public Criteria andParkingcostIsNull() {
            addCriterion("parkingcost is null");
            return (Criteria) this;
        }

        public Criteria andParkingcostIsNotNull() {
            addCriterion("parkingcost is not null");
            return (Criteria) this;
        }

        public Criteria andParkingcostEqualTo(Float value) {
            addCriterion("parkingcost =", value, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostNotEqualTo(Float value) {
            addCriterion("parkingcost <>", value, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostGreaterThan(Float value) {
            addCriterion("parkingcost >", value, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostGreaterThanOrEqualTo(Float value) {
            addCriterion("parkingcost >=", value, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostLessThan(Float value) {
            addCriterion("parkingcost <", value, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostLessThanOrEqualTo(Float value) {
            addCriterion("parkingcost <=", value, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostIn(List<Float> values) {
            addCriterion("parkingcost in", values, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostNotIn(List<Float> values) {
            addCriterion("parkingcost not in", values, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostBetween(Float value1, Float value2) {
            addCriterion("parkingcost between", value1, value2, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingcostNotBetween(Float value1, Float value2) {
            addCriterion("parkingcost not between", value1, value2, "parkingcost");
            return (Criteria) this;
        }

        public Criteria andParkingNameIsNull() {
            addCriterion("parking_name is null");
            return (Criteria) this;
        }

        public Criteria andParkingNameIsNotNull() {
            addCriterion("parking_name is not null");
            return (Criteria) this;
        }

        public Criteria andParkingNameEqualTo(String value) {
            addCriterion("parking_name =", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameNotEqualTo(String value) {
            addCriterion("parking_name <>", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameGreaterThan(String value) {
            addCriterion("parking_name >", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameGreaterThanOrEqualTo(String value) {
            addCriterion("parking_name >=", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameLessThan(String value) {
            addCriterion("parking_name <", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameLessThanOrEqualTo(String value) {
            addCriterion("parking_name <=", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameLike(String value) {
            addCriterion("parking_name like", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameNotLike(String value) {
            addCriterion("parking_name not like", value, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameIn(List<String> values) {
            addCriterion("parking_name in", values, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameNotIn(List<String> values) {
            addCriterion("parking_name not in", values, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameBetween(String value1, String value2) {
            addCriterion("parking_name between", value1, value2, "parkingName");
            return (Criteria) this;
        }

        public Criteria andParkingNameNotBetween(String value1, String value2) {
            addCriterion("parking_name not between", value1, value2, "parkingName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameIsNull() {
            addCriterion("owner_name is null");
            return (Criteria) this;
        }

        public Criteria andOwnerNameIsNotNull() {
            addCriterion("owner_name is not null");
            return (Criteria) this;
        }

        public Criteria andOwnerNameEqualTo(String value) {
            addCriterion("owner_name =", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameNotEqualTo(String value) {
            addCriterion("owner_name <>", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameGreaterThan(String value) {
            addCriterion("owner_name >", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameGreaterThanOrEqualTo(String value) {
            addCriterion("owner_name >=", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameLessThan(String value) {
            addCriterion("owner_name <", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameLessThanOrEqualTo(String value) {
            addCriterion("owner_name <=", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameLike(String value) {
            addCriterion("owner_name like", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameNotLike(String value) {
            addCriterion("owner_name not like", value, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameIn(List<String> values) {
            addCriterion("owner_name in", values, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameNotIn(List<String> values) {
            addCriterion("owner_name not in", values, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameBetween(String value1, String value2) {
            addCriterion("owner_name between", value1, value2, "ownerName");
            return (Criteria) this;
        }

        public Criteria andOwnerNameNotBetween(String value1, String value2) {
            addCriterion("owner_name not between", value1, value2, "ownerName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}