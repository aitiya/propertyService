package com.tianyac.property.pojo;

public class Parking {
    private Integer id;

    private String parkingName;

    private Integer area;

    private Integer number;

    private Integer numberRemaining;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParkingName() {
        return parkingName;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName == null ? null : parkingName.trim();
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getNumberRemaining() {
        return numberRemaining;
    }

    public void setNumberRemaining(Integer numberRemaining) {
        this.numberRemaining = numberRemaining;
    }
}