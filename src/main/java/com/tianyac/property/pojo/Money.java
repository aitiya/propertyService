package com.tianyac.property.pojo;

public class Money {
    private Integer id;

    private String idcard;

    private String name;

    private String feeitem;

    private Integer amount;

    private String times;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard == null ? null : idcard.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getFeeitem() {
        return feeitem;
    }

    public void setFeeitem(String feeitem) {
        this.feeitem = feeitem == null ? null : feeitem.trim();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times == null ? null : times.trim();
    }
}