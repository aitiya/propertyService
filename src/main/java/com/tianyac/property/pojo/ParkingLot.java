package com.tianyac.property.pojo;

public class ParkingLot {
    private Integer id;

    private String ownerIdCard;

    private String parkingtime;

    private Float parkingcost;

    private String parkingName;

    private String ownerName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwnerIdCard() {
        return ownerIdCard;
    }

    public void setOwnerIdCard(String ownerIdCard) {
        this.ownerIdCard = ownerIdCard == null ? null : ownerIdCard.trim();
    }

    public String getParkingtime() {
        return parkingtime;
    }

    public void setParkingtime(String parkingtime) {
        this.parkingtime = parkingtime == null ? null : parkingtime.trim();
    }

    public Float getParkingcost() {
        return parkingcost;
    }

    public void setParkingcost(Float parkingcost) {
        this.parkingcost = parkingcost;
    }

    public String getParkingName() {
        return parkingName;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName == null ? null : parkingName.trim();
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName == null ? null : ownerName.trim();
    }
}