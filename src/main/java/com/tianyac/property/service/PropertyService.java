package com.tianyac.property.service;

import com.tianyac.property.pojo.Property;
import com.tianyac.property.pojo.PropertyStatistics;

import java.util.List;

public interface PropertyService {

    List<Property> getPropertyList(Integer page,Integer size,String key,String keyWord);

    List<Property> getPropertyList();

    List<Property> getPropertyList(Property property);

    List<PropertyStatistics> propertyCount();

    Property getProperty(int id);

    int insertProperty(Property property);

    int deleteProperty(int id);

    int updateProperty(Property property);
}
