package com.tianyac.property.service;

import com.tianyac.property.pojo.Admin;

import java.util.List;

public interface AdminService {

    public int addAdmin(Admin admin);

    public List<Admin> getAdmin(String userName);
}
