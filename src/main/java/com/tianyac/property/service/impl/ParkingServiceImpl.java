package com.tianyac.property.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.ParkingMapper;
import com.tianyac.property.pojo.Parking;
import com.tianyac.property.pojo.ParkingExample;
import com.tianyac.property.service.ParkingService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService {
    @Resource
    ParkingMapper parkingMapper;

    @Override
    public List<Parking> getParkingList(Integer page, Integer size) {
        PageHelper.startPage(page,size);
        return parkingMapper.selectByExample(new ParkingExample());
    }

    @Override
    public List<Parking> getParkingList() {

        return parkingMapper.selectByExample(new ParkingExample());
    }

    @Override
    public Parking getParking(int id) {

        return parkingMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insertParking(Parking parking) {
        return parkingMapper.insert(parking);
    }

    @Override
    public int deleteParking(int id) {

        return parkingMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateParking(Parking parking) {
        return parkingMapper.updateByPrimaryKeySelective(parking);
    }
}
