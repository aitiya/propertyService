package com.tianyac.property.service.impl;

import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.VillageMapper;
import com.tianyac.property.pojo.Village;
import com.tianyac.property.pojo.VillageExample;
import com.tianyac.property.service.VillageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VillageServiceImpl implements VillageService {
    @Resource
    VillageMapper villageMapper;
    @Override
    public List<Village> getVillageList(Integer page,Integer size) {
        PageHelper.startPage(page, size);
        return villageMapper.selectByExample(new VillageExample());
    }

    @Override
    public List<Village> getVillageList() {
        return villageMapper.selectByExample(new VillageExample());
    }

    @Override
    public Village getVillage(int id) {
        return villageMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Village> getVillage(String villageName) {
        VillageExample villageExample = new VillageExample();
        VillageExample.Criteria criteria = villageExample.createCriteria();
        criteria.andVillageNameEqualTo(villageName);
        return villageMapper.selectByExample(villageExample);
    }

    public int insertVillage(Village village) {
        return villageMapper.insert(village);
    }

    @Override
    public int deleteVillage(int id) {
        return villageMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateVillage(Village village) {
        return villageMapper.updateByPrimaryKeySelective(village);
    }

}
