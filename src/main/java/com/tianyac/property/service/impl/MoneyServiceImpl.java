package com.tianyac.property.service.impl;

import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.MoneyMapper;
import com.tianyac.property.pojo.Money;
import com.tianyac.property.pojo.MoneyExample;
import com.tianyac.property.service.MoneyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MoneyServiceImpl implements MoneyService {
    @Resource
    MoneyMapper moneyMapper;
    @Override
    public List<Money> getMoneyList(Integer page, Integer size, String key, String keyWord) {
        MoneyExample moneyExample = new MoneyExample();

        MoneyExample.Criteria criteria = moneyExample.createCriteria();
        if(key.equals("name")){
            criteria.andNameLike("%"+keyWord+"%");
        }
        else if(key.equals("idcard")){
            criteria.andIdcardLike("%"+keyWord+"%");

        }else if(key.equals("feeitem")){
            criteria.andFeeitemLike("%"+keyWord+"%");
//        }else if(key.equals("villageName")){
//            criteria.andVillageNameLike("%"+keyWord+"%");
         }
        //criteria.andTimesBetween()
        PageHelper.startPage(page, size);
        return moneyMapper.selectByExample(moneyExample);
    }

    @Override
    public Money getMoney(int id) {
        return moneyMapper.selectByPrimaryKey(id);
    }

    public int insertMoney(Money money) {
        money.setTimes(String.valueOf(System.currentTimeMillis()));
        return moneyMapper.insert(money);
    }

    @Override
    public int deleteMoney(int id) {
        return moneyMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateMoney(Money money) {
        return moneyMapper.updateByPrimaryKeySelective(money);
    }

}