package com.tianyac.property.service.impl;

import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.GuardianMapper;
import com.tianyac.property.pojo.Guardian;
import com.tianyac.property.pojo.GuardianExample;
import com.tianyac.property.service.GuardianService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class GuardianServiceImpl implements GuardianService {
    @Resource
    GuardianMapper guardianMapper;

    @Override
    public List<Guardian> getGuardianList(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return guardianMapper.selectByExample(new GuardianExample());
    }

    @Override
    public List<Guardian> getGuardianList() {
        return guardianMapper.selectByExample(new GuardianExample());
    }

    @Override
    public Guardian getGuardian(Integer id) {
        return guardianMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Guardian> getGuardian(String guardianName) {
        GuardianExample guardianExample = new GuardianExample();
        GuardianExample.Criteria criteria = guardianExample.createCriteria();
        criteria.andNameEqualTo(guardianName);
        return guardianMapper.selectByExample(guardianExample);
    }

    @Override
    public int insertGuardian(Guardian guardian) {
        return guardianMapper.insert(guardian);
    }

    @Override
    public int deleteGuardian(Integer id) {
        return guardianMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updataGuardian(Guardian guardian) {
        return guardianMapper.updateByPrimaryKeySelective(guardian);
    }
}
