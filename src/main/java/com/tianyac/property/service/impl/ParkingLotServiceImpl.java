package com.tianyac.property.service.impl;

import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.ParkingLotMapper;
import com.tianyac.property.pojo.ParkingLot;
import com.tianyac.property.pojo.ParkingLotExample;
import com.tianyac.property.service.ParkingLotService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ParkingLotServiceImpl implements ParkingLotService {
    @Resource
    ParkingLotMapper parkingLotMapper;

    @Override
    public List<ParkingLot> getParkingLotList(Integer page, Integer size) {
        PageHelper.startPage(page,size);
        return parkingLotMapper.selectByExample(new ParkingLotExample());
    }

    @Override
    public List<ParkingLot> getParkingLotList() {
        return parkingLotMapper.selectByExample(new ParkingLotExample());
    }

    @Override
    public ParkingLot getParkingLot(int id) {
        return parkingLotMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insertParkingLot(ParkingLot parkingLot) {
        return parkingLotMapper.insert(parkingLot);
    }

    @Override
    public int deleteParkingLot(int id) {
        return parkingLotMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateParkingLot(ParkingLot parkingLot) {
        return parkingLotMapper.updateByPrimaryKeySelective(parkingLot);
    }
}
