package com.tianyac.property.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.DailyMapper;
import com.tianyac.property.pojo.Daily;
import com.tianyac.property.pojo.DailyExample;
import com.tianyac.property.service.DailyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DailyServiceImpl implements DailyService {
    @Resource
    DailyMapper dailyMapper;

    @Override
    public List<Daily> getDailyList(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return dailyMapper.selectByExample(new DailyExample());
    }

    @Override
    public List<Daily> getDailyList() {
        return dailyMapper.selectByExample(new DailyExample());
    }

    @Override
    public Daily getDaily(int id) {
        return dailyMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insertDaily(Daily daily) {
        System.out.println("-------" + JSON.toJSONString(daily));
        daily.setTime(String.valueOf(System.currentTimeMillis()));
        return dailyMapper.insertSelective(daily);
    }

    @Override
    public int deleteDaily(Integer id) {
        return dailyMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updataDaily(Daily daily) {
        return dailyMapper.updateByPrimaryKeySelective(daily);
    }
}
