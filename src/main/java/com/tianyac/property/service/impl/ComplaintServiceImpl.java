package com.tianyac.property.service.impl;

import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.ComplaintMapper;
import com.tianyac.property.pojo.Complaint;
import com.tianyac.property.pojo.ComplaintExample;
import com.tianyac.property.service.ComplaintService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public  class ComplaintServiceImpl implements ComplaintService {
    @Resource
    ComplaintMapper complaintMapper;
    @Override
    public List<Complaint> getComplaintList(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return complaintMapper.selectByExample(new ComplaintExample());
    }

    @Override
    public List<Complaint> getComplaintList() {
        return complaintMapper.selectByExample(new ComplaintExample());
    }

    @Override
    public Complaint getComplaint(int id) {

        return complaintMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insertComplaint(Complaint complaint) {

        return complaintMapper.insert(complaint);
    }

    @Override
    public int deleteComplaint(int id) {
        return complaintMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateComplaint(Complaint complaint) {
        return complaintMapper.updateByPrimaryKeySelective(complaint);
    }
}
