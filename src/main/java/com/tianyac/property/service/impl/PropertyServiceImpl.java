package com.tianyac.property.service.impl;

import com.github.pagehelper.PageHelper;
import com.tianyac.property.dao.PropertyStatisticsMapper;
import com.tianyac.property.dao.PropertyMapper;
import com.tianyac.property.pojo.Property;
import com.tianyac.property.pojo.PropertyStatistics;
import com.tianyac.property.pojo.PropertyExample;
import com.tianyac.property.service.PropertyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PropertyServiceImpl implements PropertyService {

    @Resource
    private PropertyMapper propertyMapper;
    @Resource
    private PropertyStatisticsMapper propertyCountMapper;

    @Override
    public List<Property> getPropertyList(Integer page,Integer size,String key,String keyWord) {
        PropertyExample propertyExample = new PropertyExample();
        PropertyExample.Criteria criteria = propertyExample.createCriteria();
        if(key.equals("ownerName")){
            criteria.andOwnerNameLike("%"+keyWord+"%");
        }else if(key.equals("ownerPhone")){
            criteria.andOwnerPhoneLike("%"+keyWord+"%");
        }else if(key.equals("ownerIdCard")){
            criteria.andOwnerIdCardLike("%"+keyWord+"%");
        }else if(key.equals("villageName")){
            criteria.andVillageNameLike("%"+keyWord+"%");
        }
        PageHelper.startPage(page, size);
        return propertyMapper.selectByExample(propertyExample);
    }

    @Override
    public List<Property> getPropertyList() {
        return propertyMapper.selectByExample(new PropertyExample());
    }

    @Override
    public List<Property> getPropertyList(Property property) {
        PropertyExample propertyExample = new PropertyExample();
        PropertyExample.Criteria criteria = propertyExample.createCriteria();
        if (property.getVillageName()!=null) {
            criteria.andVillageNameEqualTo(property.getVillageName());
        }
        if (property.getBuilding()!=null) {
            criteria.andBuildingEqualTo(property.getBuilding());
        }
        if (property.getRoom()!=null) {
            criteria.andRoomEqualTo(property.getRoom());
        }
        if (property.getType()!=null) {
            criteria.andTypeEqualTo(property.getType());
        }
        if (property.getOwnerName()!=null) {
            criteria.andOwnerNameLike(property.getOwnerName());
        }
        if (property.getOwnerIdCard()!=null) {
            criteria.andOwnerIdCardEqualTo(property.getOwnerIdCard());
        }
        if (property.getOwnerPhone()!=null) {
            criteria.andOwnerPhoneEqualTo(property.getOwnerPhone());
        }
        return propertyMapper.selectByExample(propertyExample);
    }

    @Override
    public List<PropertyStatistics> propertyCount() {
        return propertyCountMapper.propertyCount();
    }

    @Override
    public Property getProperty(int id) {
        return propertyMapper.selectByPrimaryKey(id);
    }

    public int insertProperty(Property property) {
        property.setRecordTime(String.valueOf(System.currentTimeMillis()));
        return propertyMapper.insert(property);
    }

    @Override
    public int deleteProperty(int id) {
        return propertyMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateProperty(Property property) {
        return propertyMapper.updateByPrimaryKeySelective(property);
    }

}
