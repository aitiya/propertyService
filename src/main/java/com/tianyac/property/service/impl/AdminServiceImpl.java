package com.tianyac.property.service.impl;

import com.tianyac.property.dao.AdminMapper;
import com.tianyac.property.pojo.Admin;
import com.tianyac.property.pojo.AdminExample;
import com.tianyac.property.service.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Resource
    AdminMapper adminMapper;

    @Override
    public int addAdmin(Admin admin) {
        return adminMapper.insertSelective(admin);
    }

    @Override
    public List<Admin> getAdmin(String userName) {
        AdminExample adminExample = new AdminExample();
        AdminExample.Criteria criteria = adminExample.createCriteria();
        criteria.andUserNameEqualTo(userName);
        return adminMapper.selectByExample(adminExample);
    }

}
