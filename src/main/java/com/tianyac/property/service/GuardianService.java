package com.tianyac.property.service;

import com.tianyac.property.pojo.Guardian;

import java.util.List;

public interface GuardianService {

    List<Guardian> getGuardianList(Integer page, Integer size);

    List<Guardian> getGuardianList();

    Guardian getGuardian(Integer id);

    List<Guardian> getGuardian(String guardianName);

    int insertGuardian(Guardian guardian);

    int deleteGuardian(Integer id);

    int updataGuardian(Guardian guardian);

}
