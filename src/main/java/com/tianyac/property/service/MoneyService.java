package com.tianyac.property.service;

import com.tianyac.property.pojo.Money;

import java.util.List;

public interface MoneyService {

    List<Money> getMoneyList(Integer page, Integer size, String key, String keyWord);

    Money getMoney(int id);

    int insertMoney(Money money);

    int deleteMoney(int id);

    int updateMoney(Money money);
}