package com.tianyac.property.service;

import com.tianyac.property.pojo.ParkingLot;

import java.util.List;

public interface ParkingLotService {
    List<ParkingLot> getParkingLotList(Integer page, Integer size);

    List<ParkingLot> getParkingLotList();

    ParkingLot getParkingLot(int id);

    int insertParkingLot(ParkingLot parkingLot);

    int deleteParkingLot(int id);

    int updateParkingLot(ParkingLot parkingLot);
}
