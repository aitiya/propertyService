package com.tianyac.property.service;

import com.tianyac.property.pojo.Parking;

import java.util.List;

public interface ParkingService {
    List<Parking> getParkingList(Integer page, Integer size);

    List<Parking> getParkingList();

    Parking getParking(int id);

    int insertParking(Parking parking);

    int deleteParking(int id);

    int updateParking(Parking parking);
}
