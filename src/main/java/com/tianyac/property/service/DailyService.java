package com.tianyac.property.service;

import com.tianyac.property.pojo.Daily;

import java.util.List;

public interface DailyService {

    List<Daily> getDailyList(Integer page, Integer size);

    List<Daily> getDailyList();

    Daily getDaily(int id);

    int insertDaily(Daily daily);

    int deleteDaily(Integer id);

    int updataDaily(Daily daily);


}
