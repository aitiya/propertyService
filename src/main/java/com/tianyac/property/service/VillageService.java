package com.tianyac.property.service;

import com.tianyac.property.pojo.Village;

import java.util.List;

public interface VillageService {

    List<Village> getVillageList(Integer page, Integer size);

    List<Village> getVillageList();

    Village getVillage(int id);

    List<Village> getVillage(String villageName);

    int insertVillage(Village village);

    int deleteVillage(int id);

    int updateVillage(Village village);
}
