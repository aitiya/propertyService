package com.tianyac.property.service;

import com.tianyac.property.pojo.Complaint;

import java.util.List;

public interface ComplaintService {
    List<Complaint> getComplaintList(Integer page, Integer size);

    List<Complaint> getComplaintList();

    Complaint getComplaint(int id);

    int insertComplaint(Complaint complaint);

    int deleteComplaint(int id);

    int updateComplaint(Complaint complaint);

}
