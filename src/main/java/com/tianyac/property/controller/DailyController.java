package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Daily;
import com.tianyac.property.pojo.Guardian;
import com.tianyac.property.service.DailyService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class DailyController {
    @Resource
    DailyService dailyService;

    /**
     *
     * @param params
     * @return
     */
    @PostMapping("/addDaily")
    public Result addDaily(@RequestBody JSONObject params){
        if(dailyService.insertDaily(JSON.parseObject(params.toJSONString(), Daily.class))==1){

            return ResultUtil.success("添加成功");
        }else {
            return ResultUtil.error(1001,"添加失败");
        }
    }

    /**
     *
     * @param id
     * @param params
     * @return
     */
    @PostMapping("/updateDaily/{id}")
    public Result updateDaily(@PathVariable("id") Integer id, @RequestBody JSONObject params){
        params.put("id",id);
        if(dailyService.updataDaily(JSON.parseObject(params.toJSONString(),Daily.class))==1){
            return ResultUtil.success("修改成功");
        }else {
            return ResultUtil.error(1001,"修改失败");
        }
    }

    /**
     *
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/getDailyList")
    public Result getDailyList(@RequestParam(name = "page") int page,@RequestParam(name = "size") int size){
        List<Daily> dailyList =dailyService.getDailyList(page,size);
        PageInfo<Daily> pageInfo = new PageInfo<>(dailyList);
        return ResultUtil.success(pageInfo);
    }

    /**
     *
     * @return
     */
    @GetMapping("/getDailyList/all")
    public Result getDailyList(){
        return ResultUtil.success(dailyService.getDailyList());
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/getDaily/{id}")
    public Result getDaily(@PathVariable("id") Integer id){
        return ResultUtil.success(dailyService.getDaily(id));
    }

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("/deleteDaily/{id}")
    public Result deleteDaily(@PathVariable("id") Integer id){
        return ResultUtil.success(dailyService.deleteDaily(id));
    }


}
