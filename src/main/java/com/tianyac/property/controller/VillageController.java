package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Village;
import com.tianyac.property.service.VillageService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class VillageController {
    @Resource
    VillageService villageService;

//    添加楼宇信息
    @PostMapping("/village")
    public Result addVillage(@RequestBody JSONObject params){
        if(villageService.insertVillage(JSON.parseObject(params.toJSONString(),Village.class))==1){
            return ResultUtil.success("添加成功");
        }else {
            return ResultUtil.error(1001,"添加失败");
        }
    }

//    修改楼宇信息
    @PostMapping("/village/{id}")
    public Result updateVillage(@PathVariable("id") Integer id,@RequestBody JSONObject params){
        params.put("id",id);
        if(villageService.updateVillage(JSON.parseObject(params.toJSONString(),Village.class))==1){
            return ResultUtil.success("修改成功");
        }else {
            return ResultUtil.error(1001,"修改失败");
        }
    }

//    分页查询楼宇信息
    @GetMapping("/village")
    public Result getVillageList(@RequestParam(name = "page") int page,@RequestParam(name = "size") int size){
        List<Village> villageList =villageService.getVillageList(page,size);
        PageInfo<Village> pageInfo = new PageInfo<>(villageList);
        return ResultUtil.success(pageInfo);
    }

//    查询所有楼宇信息
    @GetMapping("/village/all")
    public Result getVillageList(){
        return ResultUtil.success(villageService.getVillageList());
    }

    //    通过id查询指定楼宇信息
    @GetMapping("/village/hasVillageName")
    public Result getVillage(@RequestParam(name = "villageName") String villageName){
        return ResultUtil.success(villageService.getVillage(villageName));
    }

//    通过id查询指定楼宇信息
    @GetMapping("/village/{id}")
    public Result getVillage(@PathVariable("id") Integer id){
        return ResultUtil.success(villageService.getVillage(id));
    }

//    通过id删除指定楼宇信息
    @DeleteMapping("/village/{id}")
    public Result deleteVillage(@PathVariable("id") Integer id){
        return ResultUtil.success(villageService.deleteVillage(id));
    }

}
