package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tianyac.property.pojo.Admin;
import com.tianyac.property.service.AdminService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class AdminController {

    @Resource
    AdminService adminService;

    //   管理员注册
    @PostMapping("/register")
    public Result register(@RequestBody Admin admin){
        List<Admin> adminList = adminService.getAdmin(admin.getUserName());
        if(adminList.size()==0){
            if(adminService.addAdmin(admin)==1){
                return ResultUtil.success("添加成功");
            }else {
                return ResultUtil.error(1001,"添加失败");
            }
        }else {
            return ResultUtil.error(1002,"用户已存在");
        }
    }

    //    管理员登录
    @PostMapping("/login")
    public Result login(@RequestBody Admin admin){
        List<Admin> adminList = adminService.getAdmin(admin.getUserName());
        if(adminList.size()==1){
            if(adminList.get(0).getPassword().equals(admin.getPassword())){
                return ResultUtil.success(admin);
            }else {
                return ResultUtil.error(1002,"密码错误");
            }
        }else {
            return ResultUtil.error(1001,"登录失败");
        }
    }
}
