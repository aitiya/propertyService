package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Parking;
import com.tianyac.property.pojo.ParkingLot;
import com.tianyac.property.service.ParkingLotService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ParkingLotController {
    @Resource
    ParkingLotService parkingLotService;

    //添加停车信息
    @PostMapping("/parkingLot")
    public Result addParkLoting(@RequestBody JSONObject params){
        return ResultUtil.success(parkingLotService.insertParkingLot(JSON.parseObject(params.toJSONString(),ParkingLot.class)));
    }
    //    修改停车场信息
    @PostMapping("/parkingLot/{id}")
    public Result updateParkingLot(@PathVariable("id") Integer id, @RequestBody JSONObject params){
        params.put("id",id);
        return ResultUtil.success(parkingLotService.updateParkingLot(JSON.parseObject(params.toJSONString(),ParkingLot.class)));
    }

    //    分页查询停车信息
    @GetMapping("/parkingLot")
    public Result getParkingLotList(@RequestParam(name = "page") int page,@RequestParam(name = "size") int size){
        List<ParkingLot> parkingLotList =parkingLotService.getParkingLotList(page,size);
        PageInfo<ParkingLot> pageInfo = new PageInfo<>(parkingLotList);
        return ResultUtil.success(pageInfo);
    }

    //    查询所有停车信息
    @GetMapping("/parkingLot/all")
    public Result getParkingLotList(){
        List<ParkingLot> parkingLotList =parkingLotService.getParkingLotList();
        PageInfo<ParkingLot> pageInfo = new PageInfo<>(parkingLotList);
        return ResultUtil.success(pageInfo);
    }

    //    通过id查询指定停车信息
    @GetMapping("/parkingLot/{id}")
    public Result getParkingLot(@PathVariable("id") Integer id){
        return ResultUtil.success(parkingLotService.getParkingLot(id));
    }

    //    通过id删除指定停车信息
    @DeleteMapping("/parkingLot/{id}")
    public Result deleteParkingLot(@PathVariable("id") Integer id){
        System.out.println("test:"+id);
        return ResultUtil.success(parkingLotService.deleteParkingLot(id));
    }

}
