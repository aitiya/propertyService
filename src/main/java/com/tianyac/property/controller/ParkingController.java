package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Parking;
import com.tianyac.property.service.ParkingService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ParkingController {
    @Resource
    ParkingService parkingService;

    //添加停车场信息
    @PostMapping("/parking")
    public Result addParking(@RequestBody JSONObject params){
        return ResultUtil.success(parkingService.insertParking(JSON.parseObject(params.toJSONString(),Parking.class)));
    }
    //    修改停车场信息
    @PostMapping("/parking/{id}")
    public Result updateParking(@PathVariable("id") Integer id, @RequestBody JSONObject params){
        params.put("id",id);
        return ResultUtil.success(parkingService.updateParking(JSON.parseObject(params.toJSONString(),Parking.class)));
    }

    //    分页查询停车场信息
    @GetMapping("/parking")
    public Result getParkingList(@RequestParam(name = "page") int page,@RequestParam(name = "size") int size){
        List<Parking> parkingList =parkingService.getParkingList(page,size);
        System.out.println("test:"+parkingList);
        PageInfo<Parking> pageInfo = new PageInfo<>(parkingList);
        return ResultUtil.success(pageInfo);
    }

    //    查询所有停车场信息
    @GetMapping("/parking/all")
    public Result getParkingList(){
        List<Parking> parkingList =parkingService.getParkingList();
        PageInfo<Parking> pageInfo = new PageInfo<>(parkingList);
        return ResultUtil.success(pageInfo);
    }

    //    通过id查询指定停车场信息
    @GetMapping("/parking/{id}")
    public Result getParking(@PathVariable("id") Integer id){
        return ResultUtil.success(parkingService.getParking(id));
    }

    //    通过id删除指定停车场信息
    @DeleteMapping("/parking/{id}")
    public Result deleteParking(@PathVariable("id") Integer id){
        return ResultUtil.success(parkingService.deleteParking(id));
    }

}
