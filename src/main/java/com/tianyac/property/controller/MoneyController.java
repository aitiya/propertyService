package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Money;
import com.tianyac.property.service.MoneyService;
import org.springframework.web.bind.annotation.PostMapping;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
public class MoneyController {
    @Resource
    MoneyService moneyService;

    @PostMapping("/money")
    public Result addMoney(@RequestBody JSONObject params){
        System.out.println(params.toJSONString());
        return ResultUtil.success(moneyService.insertMoney(JSON.parseObject(params.toJSONString(),Money.class)));
    }

    @PostMapping("/money/{id}")
    public Result updateMoney(@PathVariable("id") Integer id,@RequestBody JSONObject params){
        params.put("id",id);
        return ResultUtil.success(moneyService.updateMoney(JSON.parseObject(params.toJSONString(),Money.class)));
    }

    @GetMapping("/money")
    public Result getMoneyList(@RequestParam(name = "page") int page,@RequestParam(name = "size") int size,@RequestParam(name = "key") String key,@RequestParam(name = "keyWord") String keyWord){
        List<Money> moneyList =moneyService.getMoneyList(page,size,key,keyWord);
        PageInfo<Money> pageInfo = new PageInfo<>(moneyList);
        return ResultUtil.success(pageInfo);
    }

    @GetMapping("/money/{id}")
    public Result getMoney(@PathVariable("id") Integer id){
        return ResultUtil.success(moneyService.getMoney(id));
    }

    @DeleteMapping("/money/{id}")
    public Result deleteMoney(@PathVariable("id") Integer id){
        return ResultUtil.success(moneyService.deleteMoney(id));
    }
}
