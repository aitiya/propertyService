package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Guardian;
import com.tianyac.property.service.GuardianService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class GuardianController {
    @Resource
    GuardianService guardianService;

    /**
     *
     * @param params
     * @return
     */
    @PostMapping("/addGuardian")
    public Result addGuardian(@RequestBody JSONObject params){
        if(guardianService.insertGuardian(JSON.parseObject(params.toJSONString(), Guardian.class))==1){

            return ResultUtil.success("添加成功");
        }else {
            return ResultUtil.error(1001,"添加失败");
        }
    }

    /**
     *
     * @param id
     * @param params
     * @return
     */
    @PostMapping("/updateGuardian/{id}")
    public Result updateGuardian(@PathVariable("id") Integer id, @RequestBody JSONObject params){
        params.put("id",id);
        if(guardianService.updataGuardian(JSON.parseObject(params.toJSONString(),Guardian.class))==1){
            return ResultUtil.success("修改成功");
        }else {
            return ResultUtil.error(1001,"修改失败");
        }
    }

    /**
     *
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/getGuardianList")
    public Result getGuardianList(@RequestParam(name = "page") int page,@RequestParam(name = "size") int size){
        List<Guardian> guardianList =guardianService.getGuardianList(page,size);
        PageInfo<Guardian> pageInfo = new PageInfo<>(guardianList);
        return ResultUtil.success(pageInfo);
    }

    /**
     *
     * @return
     */
    @GetMapping("/getGuardianList/all")
    public Result getGuardianList(){
        return ResultUtil.success(guardianService.getGuardianList());
    }

    /**
     *
     * @param guardianName
     * @return
     */
    @GetMapping("/villgetGuardianage/hasVillageName")
    public Result getGuardian(@RequestParam(name = "guardianName") String guardianName){
        return ResultUtil.success(guardianService.getGuardian(guardianName));
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/getGuardian/{id}")
    public Result getGuardian(@PathVariable("id") Integer id){
        return ResultUtil.success(guardianService.getGuardian(id));
    }

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("/deleteGuardian/{id}")
    public Result deleteGuardian(@PathVariable("id") Integer id){
        return ResultUtil.success(guardianService.deleteGuardian(id));
    }

}
