package com.tianyac.property.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Property;
import com.tianyac.property.service.PropertyService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class PropertyController {
    @Resource
    PropertyService propertyService;

//    添加房产信息
    @PostMapping("/property")
    public Result addProperty(@RequestBody JSONObject params){
        if(propertyService.insertProperty(JSON.parseObject(params.toJSONString(),Property.class))==1){
            return ResultUtil.success("添加成功");
        }else {
            return ResultUtil.error(1001,"添加失败");
        }
    }

//    修改房产信息
    @PostMapping("/property/{id}")
    public Result updateProperty(@PathVariable("id") Integer id,@RequestBody JSONObject params){
        params.put("id",id);
        if(propertyService.updateProperty(JSON.parseObject(params.toJSONString(),Property.class))==1){
            return ResultUtil.success("修改成功");
        }else {
            return ResultUtil.error(1001,"修改失败");
        }
    }

//    查询分页获取房产信息
    @GetMapping("/property")
    public Result getPropertyList(
            @RequestParam(name = "page") int page,
            @RequestParam(name = "size") int size,
            @RequestParam(name = "key") String key,
            @RequestParam(name = "keyWord") String keyWord
    ){
//        获取分页房产信息
        List<Property> propertyList =propertyService.getPropertyList(page,size,key,keyWord);
        System.out.println(propertyList.toString());
//        设置分页信息
        PageInfo<Property> pageInfo = new PageInfo<>(propertyList);
//        返回分页信息
        return ResultUtil.success(pageInfo);
    }

//    查询所有房产信息
    @GetMapping("/property/all")
    public Result getPropertyList(){
        return ResultUtil.success(propertyService.getPropertyList());
    }

//    通过id获取房产信息
    @GetMapping("/property/{id}")
    public Result getProperty(@PathVariable("id") Integer id){
        return ResultUtil.success(propertyService.getProperty(id));
    }

//    统计各个小区的人口数 -> [{villageName:"小区名称",population:"人口数量"},{villageName:"小区名称",population:"人口数量"},...]
    @GetMapping("/property/count")
    public Result propertyCount(){
        return ResultUtil.success(propertyService.propertyCount());
    }

//    模糊查询房产信息，可查询关键字 -> villageName,building,room,type,ownerName,ownerIdCard,ownerPhone
    @PostMapping("/property/searchByExample")
    public Result getProperty(@RequestBody JSONObject params){
        return ResultUtil.success(propertyService.getPropertyList(JSON.parseObject(params.toJSONString(),Property.class)));
    }

//    通过id删除房产信息
    @DeleteMapping("/property/{id}")
    public Result deleteProperty(@PathVariable("id") Integer id){
        return ResultUtil.success(propertyService.deleteProperty(id));
    }

}
