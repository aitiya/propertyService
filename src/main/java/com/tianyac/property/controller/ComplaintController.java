package com.tianyac.property.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.tianyac.property.pojo.Complaint;
import com.tianyac.property.service.ComplaintService;
import com.tianyac.property.util.Result;
import com.tianyac.property.util.ResultUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ComplaintController {
    @Resource
   ComplaintService complaintService;
//添加投诉信息
    @PostMapping("/complaint")
    public Result addComplaint(@RequestBody JSONObject params){
        return ResultUtil.success(complaintService.insertComplaint(JSON.parseObject(params.toJSONString(), Complaint.class)));
    }
//修改投诉信息
    @PostMapping("/complaint/{id}")
    public Result updateComplaint(@PathVariable("id") Integer id, @RequestBody JSONObject params){
        params.put("id",id);
        return ResultUtil.success(complaintService.updateComplaint(JSON.parseObject(params.toJSONString(),Complaint.class)));
    }
//分页查询投诉信息
    @GetMapping("/complaint")
    public Result getComplaintList(@RequestParam(name = "page") int page, @RequestParam(name = "size") int size){
        //获取分页投诉信息
        List<Complaint> complaintList =complaintService.getComplaintList(page,size);
        PageInfo<Complaint> pageInfo = new PageInfo<>(complaintList);//设置分页信息
        return ResultUtil.success(pageInfo);//返回分页信息
    }
//   查询所有投诉信息
    @GetMapping("/complaint/all")
    public Result getComlaintList(){
        List<Complaint> complaintList =complaintService.getComplaintList();
        PageInfo<Complaint> pageInfo = new PageInfo<>(complaintList);
        return ResultUtil.success(pageInfo);
    }
    @DeleteMapping("/complaint/{id}")
    public Result deleteProperty(@PathVariable("id") Integer id){
        return ResultUtil.success(complaintService.deleteComplaint(id));
    }
    @GetMapping("/complaint/{id}")
    public Result getComplaint(@PathVariable("id") Integer id){
        return ResultUtil.success(complaintService.getComplaint(id));
    }
}

