package com.tianyac.property.dao;

import com.tianyac.property.pojo.PropertyStatistics;

import java.util.List;

public interface PropertyStatisticsMapper {

    List<PropertyStatistics> propertyCount();

}
