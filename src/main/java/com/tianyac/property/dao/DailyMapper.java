package com.tianyac.property.dao;

import com.tianyac.property.pojo.Daily;
import com.tianyac.property.pojo.DailyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DailyMapper {
    long countByExample(DailyExample example);

    int deleteByExample(DailyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Daily record);

    int insertSelective(Daily record);

    List<Daily> selectByExample(DailyExample example);

    Daily selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Daily record, @Param("example") DailyExample example);

    int updateByExample(@Param("record") Daily record, @Param("example") DailyExample example);

    int updateByPrimaryKeySelective(Daily record);

    int updateByPrimaryKey(Daily record);
}