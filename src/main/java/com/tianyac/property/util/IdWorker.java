package com.tianyac.property.util;

import java.util.Random;

public class IdWorker {
    // 定义共有字符串
    private static String publicStr = "abcdefghijklmnopqrstuvwxzyABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    // 定义私有字符串
    private static String secretStr = "idworker";
    // 定义ID长度
    private static Integer IdLength = 24;

    public String worker(){
        String timeStr = String.valueOf(System.currentTimeMillis());
        String ID = timeStr;
        ID += secretStr.charAt(timeStr.charAt(timeStr.length()-1)%secretStr.length());
        while (ID.length()!=IdLength){
            for(int i = 0; i < timeStr.length(); i++){
                ID += publicStr.charAt((new Random().nextInt(50)+Integer.valueOf(timeStr.charAt(i)))%publicStr.length());
                if(ID.length()==IdLength){
                    break;
                }
            }
        }
        return ID;
    }
}
